<?php
/* Orignally written by Andrew Niemantsverdriet 
 * email: andrewniemants@gmail.com
 * website: http://www.rimrockhosting.com
 *
 * This code is on Github: https://github.com/kaptk2/portal
 *
 * Copyright (c) 2015, Andrew Niemantsverdriet <andrewniemants@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
*/

// Start a session to capure the id and url of the incomming connection
session_start();

// Setup the session variables and display errors if not set
if (isset($_GET['id'])) {
  $_SESSION['id'] = $_GET['id'];
} else {
  die("Direct Access is not allowed");
}

if (isset($_GET['url'])) {
  $_SESSION['url'] = $_GET['url'];
} else {
  $_SESSION['url'] = 'http://www.google.com';
}

// Display the login form
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Login</title>

  <!-- build:css css/index.min.css -->
  <link rel="stylesheet" href="css/kube.min.css">
  <link rel="stylesheet" href="css/master.css">
  <!-- endbuild -->

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body class="full-screen">

  <div class="row main scrollable">
    <div class="col col-9 img-area">
      <div class="promo-content">
        <div class="row gutters align-middle">
          <div class="col col-6">
            <h1 class="promo-header">Access Bank Give <span class="red">Back</span> <span class="orange">Promo</span></h1>
            <p class="promo-text">Answer this 1-minute survey and get a chance to win a Macbook Pro and a GHS 1000 to be won weekly!</p>
          </div>
          <div class="col col-6">
            <img src="images/mac_promo.png" alt="">
          </div>
        </div>

        <div class="promo-divider"></div>

        <div class="survey-form">
          <form action="" class="form">
            <div class="row auto gutters align-middle">

              <div class="col">
                <div class="form-item">
                  <label for="">Account Name</label>
                  <input type="text" placeholder="eg. Andre Carpenter">
                </div>
              </div>

              <div class="col">
                <div class="form-item">
                  <label for="">Branch</label>
                  <input type="text" placeholder="eg. KorleBu Branch">
                </div>
              </div>

              <div class="col">
                <div class="form-item form-checkboxes">
                  <label for="">Account Type</label>
                  <label class="checkbox"><input type="radio"> Current</label>
                  <label class="checkbox"><input type="radio"> Savings</label>
                </div>
              </div>

              <div class="col">
                <div class="form-item form-checkboxes">
                  <label for="">How often do you use our online service?</label>
                  <label class="checkbox"><input type="radio"> Always</label>
                  <label class="checkbox"><input type="radio"> Often</label>
                  <label class="checkbox"><input type="radio"> Seldom</label>
                  <label class="checkbox"><input type="radio"> Never</label>
                </div>
              </div>
            </div>
            <div class="row align-center">
              <div class="col col-7 push-center">
                <div class="form-item">
                  <button class="button survey w100" type="submit">Submit</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col col-3 push-middle pad-left-medium pad-right-medium">
      <div class="text-center logo-area">
        <img src="images/main-logo.png" alt="">
      </div>
      <div class="row align-center">

        <div class="col col-12 pad-medium align-center">
          <h4 class="text-center">Login Here</h4>
          <form method="post" class="form" id="login-form">
            <div class="form-item">
              <label>Phone Number <span class="req">*</span></label>
              <input type="tel" name="phonenumber" id="phonenumber" required>
              <div class="desc">Enter your phone number to login</div>
            </div>

            <div class="row pad-bottom-small">
              <div class="col col-8">
                <div class="form-item form-checkboxes">
                  <label class="checkbox"><input type="checkbox" id="terms" name="terms" value="yes"> I agree to the Terms of Service</label>
                </div>
              </div>
              <div class="col col-4">
                <button class="button primary w100" id="phone-login" type="submit">Log in</button>
              </div>
            </div>

            <div class="pad-top-small">
              <div class="message hide" data-component="message" id="loading">
                <p class="text-center" id="status">Please wait while computer is being authorized.<span class="spinner"></span></p>
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>

  <!--<footer>
    <div class="row footer">
      <div class="col col-2 offset-9 align-center push-right">
        <span class="upper display-block copyright">© Einheit Co, All Rights Reserved </span>
      </div>
    </div>
  </footer>-->


  <!--build:js js/index.min.js -->
  <script src="js/third-party/jquery.min.js"></script>
  <script src="js/third-party/jquery.validate.min.js"></script>
  <script src="js/third-party/kube.min.js"></script>
  <script type="text/javascript">
    $('#login-form').submit(function(e) {
      e.preventDefault();
      var url = '<?php echo $_SESSION['url']; ?>';
      if(!$('#terms').is(':checked')) {
        alert("You must accept the terms of service");
        return false;
      }
      $.ajax({
        type: 'POST',
        url: 'authorize.php',
        data: $('#login-form').serialize(),
        success: function(msg){
          $("#status").html(msg)
        }
      });

      $('#loading').message('open');
    });

    $('#loading').on('opened.message', function() {
      $('#status').html("Please wait while your device is being authorized <span class='spinner'></span");
    });
  </script>
  <!-- endbuild -->
</body>

</html>