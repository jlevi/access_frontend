// this is the utils object, it holds helper functions and constants
var utils = {
  /* keys*/
  USER_KEY: 'user',
  SESSION_KEY: 'session',

  /* helper functions */

  // saves an entity to local storage
  save: function(data, key){
    store(key, data);
  },

  // loads an entity from local storage
  load: function(val){
    return store.get(key);
  },

  // simple assert
  assert: function(condition, message) {
    if (!condition) throw new Error(message);
  },

  // reads a page's GET URL variables and return them as an associative array
  getURLParams: function(){
      var vars = [], hash;
      var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
      for(var i = 0; i < hashes.length; i++)
      {
          hash = hashes[i].split('=');
          vars.push(hash[0]);
          vars[hash[0]] = hash[1];
      }
      return vars;
  },

  // formats a number as money
  formatMoney: function(value, decimalPlaces, sectionsLength) {
    var re = '(\\d)(?=(\\d{' + (sectionsLength || 3) + '})+' + (decimalPlaces > 0 ? '\\.' : '$') + ')';
    return parseFloat(value).toFixed(Math.max(0, ~~decimalPlaces)).replace(new RegExp(re, 'g'), '$1,');
  },

  // shows feedback on screen
  showFeedback: function(activeSpinner, messageWidget, messageText, messageStatus, ajaxResponse){
    activeSpinner.removeClass('spinner');

    if(messageStatus === "error"){
      messageWidget.removeClass('success');
      messageWidget.removeClass('focus');
      messageWidget.addClass('error');
    }

    if(messageStatus === "success"){
      messageWidget.removeClass('error');
      messageWidget.removeClass('focus');
      messageWidget.addClass('success');
    }

    if(messageStatus === "focus"){
      messageWidget.removeClass('error');
      messageWidget.removeClass('success');
      messageWidget.addClass('focus');
    }

    if(messageStatus === "message"){
      messageWidget.removeClass('success');
      messageWidget.removeClass('primary');
      messageWidget.removeClass('error');
    }

    // the ajaxResponse should always be xhr.responseJSON of an ajax call or be undefined
    if(ajaxResponse !== undefined){
      messageWidget.html(messageText + ', response: ' + ajaxResponse.errors.msg + ')');
    } else {
      messageWidget.html(messageText);
    }

    messageWidget.removeClass('hide');
  },

  // hide a massage triggered by text input
  hideMessageViaInput: function(messageWidget, inputField){
    inputField.on("change keyup paste", function(){
      if(!messageWidget.hasClass('hide')){
        messageWidget.addClass('hide');
      }
    });
  }
};
