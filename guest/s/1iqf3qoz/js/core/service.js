// this the web service object
var service = {
  BASE_URL: '',
  PORT: '',
  PROD_URL: '',
  DEV_URL:'http://127.0.0.1',

  configure: function() {
    if(config.serveStatus === "prod"){ service.BASE_URL = service.PROD_URL + ':' + service.PORT; } // production env
    else if(config.serveStatus === "dev"){ service.BASE_URL = service.DEV_URL + ':' + service.PORT; } // dev env
    else if(config.serveStatus === "loc-prod"){ service.BASE_URL = service.DEV_URL + ':' + service.PORT; } // local run of production env
  },

  login: function (email, password){
    var xhr = $.ajax({
      method: "POST",
      url: service.BASE_URL + "/sessions",
      contentType: "application/json",
      dataType: 'json',
      data: JSON.stringify({
        email: email,
        password: password
      })
    });

    return xhr;
  }
};

service.configure();
