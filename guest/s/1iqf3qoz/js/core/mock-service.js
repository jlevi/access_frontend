$.mockjax({
  url: service.BASE_URL + "/sessions",
  contentType: "application/json",
  data: function(email, password) {
    // assert the values of the params
    if(email !== null && password !== null){
      return true;
    }
  },
  responseTime: [250, 1250], //simulates network latency
  //status: 500, //trigger an error state
  responseText: {
    token: "2WQD1A56",
    accessLevel: "documentor",
    lastModified: "2016-08-31 05:03:28",
    createdOn: "2016-08-31 05:03:28",
    expiry: "2016-11-31 05:03:28",
    links: {
      user: "sessions/2WQD1A56/user/2FCHSL45"
    }
  }
});

// selectively disable specific mockjax calls
// $.mockjax.clear(0);

// or clear the entire mockjas setup (turn on for production)
// $.mockjax.clear();
