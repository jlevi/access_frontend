// toggle the deployment state
var config = {
  serveStatus: "dev", // options are: dev (dev env), prod (production env), loc-prod (local test run of production env)
  decimalPlaces: 3 // global decimal places value
};

// sets up the date picker widget

$('.datepicker').pikaday({
 incrementMinuteBy: 1,
 showTime: true,
 showMinutes: true,
 use24hour: true,
 format: 'Do MMMM YYYY', // other formats: 'Do MMMM YYYY', 'D MMM YYYY', 'YYYY-MM-DD HH:mm:ss'
 autoClose: true, // set to false when debuggging or editin pikaday style
 theme: 'kube-theme'
});

// disable the default form sumbit action
$('form').on('submit', function(e){
   e.preventDefault();
});

// set the log levels (NB: set to 'error' for production env)
if(config.serveStatus === "prod"){ log.setLevel('ERROR'); }
else if(config.serveStatus === "dev" || config.serveStatus === "loc-prod"){ log.setLevel('DEBUG'); }
