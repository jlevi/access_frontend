$(document).ready(function() {
  var files;

  function prepareUpload(event) {
    files = event.target.files;
  }

  $('input[name=select-file]').on('change', prepareUpload);

  $('#upload').on('click', function(e){
    e.preventDefault();

    // create formData object
    var data = new FormData();
    data.append("file", files[0])

    // $.each(files, function(key, value){
    //   data.append(key, value);
    // });


    $('#upload').addClass('spinner');

    $.ajax({
        url: 'http://localhost:24356/upload',
        type: 'POST',
        data: data,
        cache: false,
        headers: {
          "Authorization": "Token 9zaSoqnX4 "
        },
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false // Set content type to false as jQuery will tell the server its a query string request
    }).done(function(data) {
      log.info("data is", data);

      $('#upload').removeClass('spinner');
    }).fail(function(xhr){
      $('#upload').removeClass('spinner');

      if(xhr.responseJSON !== undefined){
        log.info('Failed to send feedback, please check the connection and retry' + ' (' + xhr.responseJSON.errors.msg + ')');
      } else {
        log.info('Failed to send feedback, please check the connection and retry');
      }
    });
  });
});
